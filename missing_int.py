def missing_int(arr):
    smallest = 1
    for num in arr:
        if(num == smallest):
            smallest += 1
    
    return smallest
    

if __name__ == '__main__':
    list = []

    while True:
        user_input = input('Enter a number: ')
        if user_input == '':
            print('User pressed enter')
            break
        
        list.append(int(user_input))

    list.sort()
    print(missing_int(list))

