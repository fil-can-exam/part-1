def find_divisible(a, b, k):
    cnt = 0

    while True:
        if (a == b):
            break
        if (a % k == 0):
            cnt += 1
        a += 1

    print(cnt)

if __name__ == '__main__':
    start_point = 0
    end_point = 0
    divisible = 0

    start_point = input('Enter start point: ')
    end_point = input('Enter end point: ')
    divisible = input('Enter divisible: ')

    find_divisible(int(start_point), int(end_point), int(divisible))